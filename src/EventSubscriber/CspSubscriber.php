<?php

namespace Drupal\ie9\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\csp\Csp;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Alter CSP policy for IE9 Compatibility.
 */
class CspSubscriber implements EventSubscriberInterface {

  /**
   * The Config Factory Service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    if (!class_exists(CspEvents::class)) {
      return [];
    }

    $events[CspEvents::POLICY_ALTER] = ['onCspPolicyAlter'];
    return $events;
  }

  /**
   * Ie9CspSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The State service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, StateInterface $state) {
    $this->configFactory = $configFactory;
    $this->state = $state;
  }

  /**
   * Alter CSP policy for compatibility with IE9 if needed.
   *
   * Since checking the actual number of stylesheets included on the page is
   * more difficult, just check the optimization settings, as in
   * HtmlResponseAttachmentsProcessor::processAssetLibraries()
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $alterEvent
   *   The Policy Alter event.
   *
   * @see https://www.drupal.org/node/2993171
   * @see CssCollectionRenderer::render()
   * @see HtmlResponseAttachmentsProcessor::processAssetLibraries()
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent) {
    if (
      defined('MAINTENANCE_MODE')
      ||
      $this->state->get('system.maintenance_mode')
      ||
      !$this->configFactory->get('system.performance')->get('css.preprocess')
    ) {
      $policy = $alterEvent->getPolicy();
      $policy->fallbackAwareAppendIfEnabled('style-src', [Csp::POLICY_UNSAFE_INLINE]);
      $policy->fallbackAwareAppendIfEnabled('style-src-elem', [Csp::POLICY_UNSAFE_INLINE]);
    }
  }

}
