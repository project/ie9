<?php

namespace Drupal\Tests\ie9\Unit\EventSubscriber;

use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\State\StateInterface;
use Drupal\csp\Csp;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Drupal\csp\EventSubscriber\CoreCspSubscriber;
use Drupal\ie9\EventSubscriber\CspSubscriber;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for Content Security Policy events integration.
 *
 * @group ie9
 * @coversDefaultClass \Drupal\ie9\EventSubscriber\CspSubscriber
 */
class CspSubscriberTest extends UnitTestCase {

  /**
   * The State service.
   *
   * @var \Drupal\Core\State\StateInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  private $state;

  /**
   * The response object.
   *
   * @var \Drupal\Core\Render\HtmlResponse|\PHPUnit\Framework\MockObject\MockObject
   */
  private $response;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    if (!class_exists(Csp::class)) {
      $this->markTestSkipped('Content Security Policy module is not available.');
    }

    $this->state = $this->getMockBuilder(StateInterface::class)
      ->getMock();

    $this->response = $this->getMockBuilder(HtmlResponse::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Check that the subscriber listens to the Policy Alter event.
   *
   * @covers ::getSubscribedEvents
   */
  public function testSubscribedEvents() {
    $this->assertArrayHasKey(CspEvents::POLICY_ALTER, CoreCspSubscriber::getSubscribedEvents());
  }

  /**
   * Shouldn't alter the policy if no directives are enabled.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testNoDirectives() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => FALSE,
      ],
    ]);
    $this->state->expects($this->any())
      ->method('get')
      ->with(
        $this->equalTo('system.maintenance_mode')
      )
      ->willReturn(FALSE);

    $policy = new Csp();
    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertFalse($alterEvent->getPolicy()->hasDirective('script-src'));
    $this->assertFalse($alterEvent->getPolicy()->hasDirective('script-src-attr'));
    $this->assertFalse($alterEvent->getPolicy()->hasDirective('script-src-elem'));
  }

  /**
   * Policy shouldn't be altered when not needed.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testStyleNotNeeded() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => TRUE,
      ],
    ]);
    $this->state->expects($this->any())
      ->method('get')
      ->with(
        $this->equalTo('system.maintenance_mode')
      )
      ->willReturn(FALSE);

    $policy = new Csp();
    $policy->setDirective('default-src', [Csp::POLICY_ANY]);
    $policy->setDirective('style-src', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-attr', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-elem', [Csp::POLICY_SELF]);

    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertArrayEquals(
      [Csp::POLICY_SELF],
      $alterEvent->getPolicy()->getDirective('style-src')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF],
      $alterEvent->getPolicy()->getDirective('style-src-attr')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF],
      $alterEvent->getPolicy()->getDirective('style-src-elem')
    );
  }

  /**
   * Test that enabled style directives are modified when preprocess disabled.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testStyleNoPreprocess() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => FALSE,
      ],
    ]);
    $this->state->expects($this->any())
      ->method('get')
      ->with(
        $this->equalTo('system.maintenance_mode')
      )
      ->willReturn(FALSE);

    $policy = new Csp();
    $policy->setDirective('default-src', [Csp::POLICY_ANY]);
    $policy->setDirective('style-src', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-attr', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-elem', [Csp::POLICY_SELF]);

    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF],
      $alterEvent->getPolicy()->getDirective('style-src-attr')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src-elem')
    );
  }

  /**
   * Test that enabled style directives are modified in maintenance mode.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testStyleMaintenance() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => TRUE,
      ],
    ]);
    $this->state->expects($this->any())
      ->method('get')
      ->with(
        $this->equalTo('system.maintenance_mode')
      )
      ->willReturn(TRUE);

    $policy = new Csp();
    $policy->setDirective('default-src', [Csp::POLICY_ANY]);
    $policy->setDirective('style-src', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-attr', [Csp::POLICY_SELF]);
    $policy->setDirective('style-src-elem', [Csp::POLICY_SELF]);

    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF],
      $alterEvent->getPolicy()->getDirective('style-src-attr')
    );
    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src-elem')
    );
  }

  /**
   * Test style-src-elem fallback if style-src enabled.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testStyleElemFallback() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => FALSE,
      ],
    ]);


    $policy = new Csp();
    $policy->setDirective('default-src', [Csp::POLICY_ANY]);
    $policy->setDirective('style-src', [Csp::POLICY_SELF]);


    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src')
    );
    $this->assertFalse($alterEvent->getPolicy()->hasDirective('style-src-attr'));
    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      array_unique($alterEvent->getPolicy()->getDirective('style-src-elem'))
    );
  }

  /**
   * Test style-src-elem fallback if default-src enabled.
   *
   * @covers ::onCspPolicyAlter
   */
  public function testStyleDefaultFallback() {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit_Framework_MockObject_MockObject $configFactory */
    $configFactory = $this->getConfigFactoryStub([
      'system.performance' => [
        'css.preprocess' => FALSE,
      ],
    ]);


    $policy = new Csp();
    $policy->setDirective('default-src', [Csp::POLICY_SELF]);

    $alterEvent = new PolicyAlterEvent($policy, $this->response);

    $subscriber = new CspSubscriber($configFactory, $this->state);
    $subscriber->onCspPolicyAlter($alterEvent);

    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      $alterEvent->getPolicy()->getDirective('style-src')
    );
    $this->assertFalse($alterEvent->getPolicy()->hasDirective('style-src-attr'));
    $this->assertArrayEquals(
      [Csp::POLICY_SELF, Csp::POLICY_UNSAFE_INLINE],
      array_unique($alterEvent->getPolicy()->getDirective('style-src-elem'))
    );
  }

}
